import re
import os
from indexer.utils import *
from indexer.citations import BibleCitation

long_dash = '–'


def generate_bible_object(bible_books_input):
    bible = {}

    with open(bible_books_input, 'r') as input:
        for line in input:
            line = line.rstrip()
            name, abbreviations = line.split(': ')
            if ',' in abbreviations:
                abbreviations = abbreviations.split(', ')

            bible[name] = BibleCitation(name, abbreviations)

    return bible


def parse_page_bible_citations(filename, bible, page, debug=False, verbose_debug=False):
    with open(filename, 'r', encoding='utf-8') as input:
        for line in input:

            # Hacky - If verbose_debug is turned on, this is going to be a performance hit - we repeat the same logic
            # I believe with the way I'm outputting files, sentences should not be split between lines
            if verbose_debug:
                sentence_regex = re.compile(r'[A-Z][^.!?]+[.!?]|\([A-Z][^)]+\)')

                sentences = sentence_regex.findall(line)
                for s in sentences:
                    found_citation = False
                    for book in bible.values():
                        citation_sentences = book.full_regex.search(s)
                        if citation_sentences:
                            found_citation = True
                            print('"%s"' % s)
                            break

            for book in bible.values():
                assert isinstance(book, BibleCitation)
                results = book.full_regex.finditer(line)
                for r in results:
                    book.add_verse_mapping(r, page, debug)


def populate_bible_citations(pages_dir, bible, debug=False, verbose_debug=False):

    # Iterate through the sorted pages
    for page_file in sorted(os.listdir(pages_dir), key=lambda file_name: (get_pagenum_from_filename(file_name))):
        page = get_pagenum_from_filename(page_file)
        if debug:
            print('------- Page %s ------- ' % page)
        parse_page_bible_citations(pages_dir + page_file, bible, page, debug, verbose_debug)
        if debug:
            print()


def print_bible_citations(bible):
    for book in bible.values():
        if not book.verse_mapping:
            continue

        print(book.full_name)
        for chapter_verse in sorted(book.verse_mapping):
            print(str(chapter_verse) + ' - ', end='')
            print(book.verse_pages(chapter_verse))
        print()


def main():

    ORIG_DOCX = 'manuscript/chapters-mod.docx'
    ZIP_DOCX = 'manuscript/chapters-mod.zip'
    UNZIPPED_DIR = 'manuscript/unzipped/'
    FULL_TEXT = 'manuscript/full_text.txt'
    UTF_PARAGRAPHS = 'output/utf_paragraphs.txt'
    DOC_XML = 'manuscript/unzipped/word/document.xml'
    FOOTNOTES_XML = 'manuscript/unzipped/word/footnotes.xml'
    PAGES_DIR = 'manuscript/pages/'
    FOOTNOTES_DIR = 'manuscript/footnotes/'
    FOOTNOTED_PAGES_DIR = 'manuscript/footnoted-pages/'
    BIBLE_BOOKS = 'input/bible_books.txt'

    #unzip_docx(ORIG_DOCX, ZIP_DOCX, UNZIPPED_DIR)
    #output_fulltext(ORIG_DOCX, FULL_TEXT, debug=True)
    #output_non_utf(ORIG_DOCX, UTF_PARAGRAPHS, debug=True)
    #output_by_page(DOC_XML, PAGES_DIR, debug=True)
    #output_footnotes(FOOTNOTES_XML, FOOTNOTES_DIR, debug=True)
    #output_footnoted_pages(PAGES_DIR, FOOTNOTES_DIR, FOOTNOTED_PAGES_DIR, debug=True)

    bible = generate_bible_object(BIBLE_BOOKS)

    populate_bible_citations(FOOTNOTED_PAGES_DIR, bible, debug=True, verbose_debug=True)
    print_bible_citations(bible)


if __name__ == '__main__':
    main()
