import os
from indexer.utils import *
from indexer.language_utils import *
from nltk.tokenize import sent_tokenize

def main():
    #FOOTNOTED_PAGES_DIR = 'manuscript/footnoted-pages/'
    PAGES_DIR = 'manuscript/pages/'
    test_words = [
        'Athanasius',
        'final cause',
        'formal cause',
        'Barth',
        'body',
        'soul',
        'Adam',
        'Garden of Eden',
        'Genesis',
        'calvinism',
        'Sorge',
        'aging',
        'caloric restriction',
        'fasting',
    ]
    test_words = [
        'deus sive natura'
    ]

    #test_words = ['fasting', 'caloric restriction']
    #test_words = ['caloric restriction']
    verbose_debug = True

    for word in test_words:
        regex_results, token_results = set(), set()

        if verbose_debug:
            if isinstance(word, collections.abc.Sequence) and not isinstance(word, str):
                word_rep = '(' + ','.join(w.strip() for w in word) + ')'
            else:
                word_rep = word
            print('------- %s -------' % word_rep)
            sentence_results = []

            # Sentence tokenizer doesn't correctly split on these
            dot_paren = '.”'

        # Iterate through the sorted pages
        for page_file in sorted(os.listdir(PAGES_DIR), key=lambda file_name: get_pagenum_from_filename(file_name)):
            page = get_pagenum_from_filename(page_file)

            with open(PAGES_DIR + page_file, 'r', encoding='utf-8') as infile:
                for line in infile:

                    # Search the entire paragraph by regular expression
                    if word_regex_match(word, line):
                        regex_results.add(page)

                        # Running the regex twice may be a performance hit, but I don't want the others to be
                        # dependent upon the tokenizer, either
                        if verbose_debug:
                            for s in sent_tokenize(line):
                                for p in arrayfix_sentence_tokenizer(s):
                                    if word_regex_match(word, p):
                                        sentence_results.append('Page %s - %s' % (page, p))

                    # Splits the paragraph into sentences and then looks for a match
                    if word_token_match(word, line):
                        token_results.add(page)

        print(word, '-', pretty_pages(token_results.union(regex_results)))
        if regex_results != token_results:
            regex_unique = regex_results.difference(token_results)
            token_unique = token_results.difference(regex_results)
            if regex_unique:
                print('- Regex-only:', pretty_pages(regex_unique))
            if token_unique:
                print('- Token-only:', pretty_pages(token_unique))

        # Print all the matched sentences
        if verbose_debug:
            for s in sentence_results:
                print(s)
            print()


if __name__ == '__main__':
    main()