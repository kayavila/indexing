import docx
import operator
import nltk, collections
from nltk.collocations import *

input_file = 'manuscript/full_text.txt'

if __name__ == '__main__':

    frequencies = collections.Counter()

    with open(input_file, 'r', encoding='utf-8') as infile:
        raw = infile.read()
        tokens = nltk.word_tokenize(raw)

    #print(list(nltk.bigrams(tokens)))
    common_words = ['as', 'if', 's', '’', 'the', 'and', 'a', 'are', 'for', ',', 'in', 'do', 'was', '.', 'the',
                    'indeed', 'to', 'be', 'might', 'has', 'been', 'it', 'is', 'could', 'that', 'this', 'macron',
                    '(', ')', '“', 'may', 'of', 'he', 'with', 'its', 'no', 'did', 'not', '[', ']', 'o', 'those', 'who',
                    'on', 'by', 'his', '”', 'but', 'or', 'not', 'from', 'through', 'we', 'our', 'more', 'at', ';',
                    'were', 'have', 'so', 'will', 'more', 'by', 'however', 'though', 'which', 'will', ':', 'not',
                    'through', 'only', 'his', 'her', 'into', 'under', 'at', 'what', 'would', 'into', 'such', 'have',
                    'an', 'a', 'aging', 'had', 'any', 'own', 'their', 'also', 'made', 'when', 'these', 'than', 'had',
                    'himself', 'noted', 'them', 'all', 'about', 'should', 'they', 'any', 'while', 'one', 'both',
                    'over', 'where', 'too', 'can', 'even', 'increasingly', 'must', 'how', 'between', 'better', 'upon',
                    'without', 'whether', 'itself', 'once', 'again', 'less', 'more', 'much', 'us', 'reminds', 'says',
                    'why', 'first', 'primary', 'him', 'later', 'other', 'little', 'respects', 'says', 'per', 'readily',
                    'out', 'd.', 'et', 'percent', 'us', 'nicely', 'far', 'less', 'against', 'i', 'brief', 'you', 'very',
                    'saw', 'does', 'gradually', 'asserted', 'does', '?', 'viewed', 'every', 'good', 'means', 'spoke',
                    'continued', 'earlier', 'my', 'now', 'because', 'like', 'spoke', 'earlier', 'come', 'days', 'Unlike',
                    'notes', 'regarding', 'being', 'after', 'before', 'particularly', 'then', 'there', 'thing', 'same',
                    'very', 'having', 'likely', 'finally', 'becomes', 'describes', 'believed', 'then', 'asserted', 'put',
                    'useful', 'prove', 'observes', 'observed', 'finely', 'toward', 'current', 'mentioned', 'each',
                    'entitled', 'new', 'seemingly', 'asserts', 'asserted', 'results', 'off', 'some', 'already', 'neither',
                    'here', 'either', 'given', 'emphasized', 'emphasizes', 'obscures', 'obscured', 'reports', 'reported',
                    'each', 'nor', 'enough']

    # See https://towardsdatascience.com/collocations-in-nlp-using-nltk-library-2541002998db?gi=3a9f3b71141e

    # bigram = nltk.collocations.BigramAssocMeasures()
    # finder = BigramCollocationFinder.from_words(tokens)
    # finder.apply_freq_filter(2)
    # count = 0
    # for i in finder.score_ngrams(bigram.likelihood_ratio):
    #     first, second = i[0][0], i[0][1]
    #     if first.lower() in common_words or second.lower() in common_words:
    #         continue
    #     else:
    #         print(first, second)
    #         count += 1
    #
    # print('Bigram total count:', count)


    trigrams = nltk.collocations.TrigramAssocMeasures()
    finder = TrigramCollocationFinder.from_words(tokens)
    finder.apply_freq_filter(2)
    count = 0
    for i in finder.score_ngrams(trigrams.likelihood_ratio):
        first, second, third = i[0][0], i[0][1], i[0][2]
        if first.lower() in common_words or second.lower() in common_words or third.lower() in common_words:
            continue
        else:
            print(first, second, third)
            count += 1

    print('Trigram total count:', count)


    #print('Those are the words and their frequency of occurrence in the book:')
    #for f in sorted(frequencies.items(), key=operator.itemgetter(1), reverse=True):
    #    print(f[0], f[1])
