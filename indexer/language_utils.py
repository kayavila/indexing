import os
import re
import collections.abc
from nltk.tokenize import sent_tokenize
from indexer.utils import get_pagenum_from_filename
from indexer.tokenizer import expanded_word_tokenize

regexes = {}


def _iterate_by_method():
    pass


def arrayfix_sentence_tokenizer(text):
    dot_paren = '.”'
    text_parts, results = [], []
    while dot_paren in text:
        first_part = text.split(dot_paren)[0] + dot_paren
        text_parts.append(first_part)
        text = text.replace(first_part, '').lstrip()

    # We need to either append the whole thing or the last part from the loop above
    text_parts.append(text)

    # Now we need to send it through for the regular tokenizing
    for p in text_parts:
        results.extend(sent_tokenize(p))

    return results


def word_regex_match(word, text, case_sensitive=False):

    # If we're given a list or set, need to handle this differently and match either one
    if isinstance(word, collections.abc.Sequence) and not isinstance(word, str):
        word = '(?:' + '|'.join(w.strip() for w in word) + ')'

    if not case_sensitive:
        word = word.lower()
        text = text.lower()
    if word not in regexes:
        #TODO It used to be %s? - why...?
        regexes[word] = re.compile(r'(^|[\s\-:\'\"‘“(—])%s($|[\s\-\'\"’”,:;.?!—)])' % word)
        print(regexes[word])
    return regexes[word].search(text)


def word_token_match(word, text, case_sensitive=False):

    def _strip_punctuation(word):
        return re.sub(r'[.,?!“”‘’;()]', '', word)

    if isinstance(word, str):
        words = (word,)
    else:
        words = word

    for word in words:
        phrase = None
        if not case_sensitive:
            word = word.lower()
            text = text.lower()
        if ' ' in word:
            phrase = word.split()

            # Only handling two-word phrases currently
            #if len(phrase) > 2:
            #    raise NotImplementedError

        # Running into an issue where we don't split sentences on .", so handle that separately
        for p in arrayfix_sentence_tokenizer(text):
            previous_words = []
            for w in expanded_word_tokenize(p):
                w = _strip_punctuation(w)
                if not phrase:
                    if w == word:
                        return True

                # TODO - This needs to be updated to handle two-word phrases
                else:
                    #print(previous_words)
                    if w:
                        previous_words.append(w)
                    if len(previous_words) >= len(phrase):
                        if previous_words[-len(phrase):] == phrase:
                            print()
                            return True

    return False
