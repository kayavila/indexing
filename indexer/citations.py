import re
from functools import total_ordering
from indexer.utils import pretty_pages


long_dash = '–'


class InvalidChapterVerseError(Exception):
    pass


@total_ordering
class ChapterVerse:
    def __init__(self, start_chapter, start_verse=None, verse_letter=None, end_chapter=None, end_verse=None):
        # Based on discussion, we should never see...
        # Start chapter, start verse, and end chapter (without end verse) - John 3:16-4
        # Or start chapter, end chapter, and end verse (without start verse) - John 3-4:1
        if (start_verse and end_chapter and not end_verse) or (end_chapter and end_verse and not start_verse):
            raise InvalidChapterVerseError

        # Currently, verse_letters are only supported with a verse and no end_chapter or end_verse
        if verse_letter and (end_chapter or end_verse):
            raise NotImplementedError

        self.end_chapter, self.end_verse, self.verse, self.verse_letter = None, None, None, None
        self.verse_letter = verse_letter
        self.chapter = int(start_chapter)
        # Cast optional arguments to ints if they exist
        if start_verse:
            self.verse = int(start_verse)
        if end_verse:
            self.end_verse = int(end_verse)
        if end_chapter:
            self.end_chapter = int(end_chapter)

    def __str__(self):
        result = str(self.chapter)
        if self.verse:
            result += ':%s' % str(self.verse)
            if self.verse_letter:
                result += self.verse_letter
        if self.end_chapter:
            result += '%s%s' % (long_dash, self.end_chapter)
        if self.end_verse:
            result += '%s%s' % (long_dash, self.end_verse)
        return result

    def __repr__(self):
        return '<ChapterVerse %s>' % self.__str__()

    def __hash__(self):
        return hash(self.__str__())

    def __eq__(self, other):
        return (self.chapter == other.chapter and self.verse == other.verse and self.end_chapter == other.end_chapter
                and self.end_verse == other.end_verse and self.verse_letter == other.verse_letter)

    def __ne__(self, other):
        return not (self == other)

    def __lt__(self, other):

        if self == other:
            return False

        # If chapters are unequal, lower number will always be less.  Chapter is required.
        if self.chapter != other.chapter:
            return self.chapter < other.chapter

        # Chapters must be equal
        assert (self.chapter == other.chapter)

        # If we're here, chapter must be equal.
        if self.verse != other.verse:
            if self.verse and other.verse:
                # If they both have verses, lower verse will always be less
                return self.verse < other.verse
            elif not self.verse and other.verse:
                # If only the other has a verse, self is lower
                return True
            elif self.verse and not other.verse:
                # If only self has a verse, other is lower
                return False
            else:
                # We should never hit this
                raise NotImplementedError

        # Either the verses are the same, or neither has a verse
        assert (self.verse == other.verse)

        # If one has a verse letter, and the other does not, then the one with letter is less
        if self.verse_letter:
            if self.end_verse or self.end_chapter:
                # Verse letters are only supported on single chapter verse
                raise NotImplementedError
            return True
        elif other.verse_letter:
            if other.end_verse or other.end_chapter:
                # Verse letters are only supported on single chapter verse
                raise NotImplementedError
            return False

        # Neither should have an end chapter.
        assert self.end_chapter is None
        assert other.end_chapter is None

        # Check for when chapters are equal and neither has a verse.
        if not self.verse and not other.verse:
            if self.end_chapter and other.end_chapter:
                # If they both have end chapters, lower will always be less
                return self.end_chapter < other.end_chapter
            elif not self.end_chapter and other.end_chapter:
                # If only the other has an end chapter, self is less
                return True
            elif self.end_chapter and not other.end_chapter:
                # If only self has an end chapter, other is lower
                return False
            else:
                # We should never hit this
                raise NotImplementedError

        # Verses must be populated (we already know they're equal)
        assert self.verse is not None
        assert other.verse is not None

        # Check if verses are equal
        if self.verse == other.verse:
            if self.end_verse and not other.end_verse:
                return False
            elif not self.end_verse and other.end_verse:
                return True
            else:
                return self.end_verse < other.end_verse

        # We should never reach here
        raise NotImplementedError


class BibleCitation:

    numbered_book_regex = re.compile(r'\s*[1-3]\s*(Sam|King|Kgs|Chr|Cor|Thess|Tim|Pet|John)')
    verse_letter_regex = re.compile(r'[a-j]$')

    def __init__(self, name, abbreviations):
        self.verse_mapping = {}
        self.full_name = name

        if not isinstance(abbreviations, list):
            abbreviations = [abbreviations]
        self.abbreviations = abbreviations

        # Create the regex
        abbreviations.append(self.full_name)

        # Some of the extra spaces are in there specifically for this typo: 1 Cor 6: 9–11
        self.full_regex = re.compile('(' + '|'.join(abbreviations) + r')\s+' +
                                     r'[1-9][0-9]*(:\s?[1-9][0-9]*[a-z]?)?' +
                                     r'([-–—][1-9][0-9]*(:\s?[1-9][0-9]*)?)?' +
                                     r'([,;]\s*[1-9][0-9]*(:\s?[1-9][0-9]*)?(-[1-9][0-9]*(:[1-9][0-9]*)?)?)*' +

                                     # Plus, I need to catch if a trailing 1, 2, or 3 belongs to another book
                                     r'(\s*(Sam|King|Kgs|Chr|Cor|Thess|Tim|Pet|John))?')

    def add_verse_mapping(self, text, page, debug=False, silence_citation_text=False):

        if isinstance(text, re.Match):
            text = text.group(0)
        assert isinstance(text, str)

        # Hacky way to not print multi-part citations, but still get the text before modifications
        if debug and not silence_citation_text and (',' not in text and ';' not in text):
            print('Citation text: "%s"' % text)
        elif debug and not silence_citation_text and (',' in text or ';' in text):
            print('Multi-part text: "%s"' % text)

        # There's a mix of short and long dashes (and a couple extra long), standardize on short
        text = str.replace(text, '–', '-')
        text = str.replace(text, '—', '-')

        # There's also a mix of commas and semi-colons, standardize on semi-colon
        text = str.replace(text, ',', ';')

        # We only need this for debugging messages (and for multi-part citations)
        if self.numbered_book_regex.match(text):
            num, book = text.split()[0], text.split()[1]
            book = '%s %s' % (num, book)
        else:
            book = text.split()[0]

        # Now we have to figure out if this is multiple citations
        # e.g. Galatians 1:4; 2:20 or John 6:35, 48
        if ';' in text:
            text = ' '.join(text.split()[1:])
            citations = text.split(';')
            chapter = None
            for c in citations:
                c = c.strip()

                # If this is the next citation, e.g. 1 John, skip it
                if not self.numbered_book_regex.match(c):
                    # This will get rewritten as needed, but I'll need it for the *next* one
                    if ':' in c:
                        chapter = c.split(':')[0]
                    else:
                        verse = c
                        c = '%s:%s' % (chapter, verse)

                    self.add_verse_mapping('%s %s' % (book, c), page, debug=debug, silence_citation_text=True)
            return
        else:
            # Strip off the book name and continue
            text = text.replace(book, '').strip()

        verse, end_chapter, end_verse, letter = None, None, None, None
        if ':' not in text:
            # Starting to build out the logic
            if '-' not in text:
                chapter = text
            else:
                chapter, end_chapter = text.split('-')
        elif '-' not in text:
            chapter, verse = text.split(':')
            if self.verse_letter_regex.search(verse):
                letter = verse[-1]
                verse = verse.rstrip(letter)

            chapter, verse = int(chapter), int(verse)
        else:
            part_one, part_two = text.split('-')
            chapter, verse = part_one.split(':')
            chapter, verse = int(chapter), int(verse)
            if ':' in part_two:
                end_chapter, end_verse = text.split(':')
                end_chapter = int(end_chapter)
                end_verse = int(end_verse)
            else:
                end_verse = int(part_two)

        # Create the object
        chapter_verse = ChapterVerse(chapter, verse, letter, end_chapter, end_verse)

        # Now, increment the pages if appropriate
        if chapter_verse not in self.verse_mapping:
            self.verse_mapping[chapter_verse] = set()

        assert isinstance(self.verse_mapping[chapter_verse], set)
        self.verse_mapping[chapter_verse].add(page)

        if debug:
            print('-', book, chapter_verse)


    def verse_pages(self, chapter_verse):
        pages = []
        if not self.verse_mapping[chapter_verse]:
            return pages

        pages = [p for p in sorted(self.verse_mapping[chapter_verse])]

        result = ''
        previous_page = -1
        for p in pages:
            if p == (previous_page + 1):
                # This is part of a series - remove the space and comma, add a dash
                if not result.endswith('-'):
                    result = result.rstrip(' ').rstrip(',')
                    result += '-'
            elif result.endswith('-'):
                # The previous series needs to close
                result += str(previous_page) + ', '
                result += str(p) + ', '
            else:
                result += str(p) + ', '

            previous_page = p

        # Need to catch an end case where we haven't ended the series because there wasn't another
        if result.endswith('-'):
            result += str(previous_page)

        return result.rstrip(', ')
