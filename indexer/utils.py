import os, shutil, codecs
import re
from zipfile import ZipFile
from docx import Document
import xml.etree.ElementTree as ET


class InvalidMacronParsingError(Exception):
    pass


class InvalidUmlautParsingError(Exception):
    pass


def get_pagenum_from_filename(name):
    # I'm inconsistent and sometimes don't have '.txt'
    name = name.split('.')[0]
    return int(name.split('_')[-1])


def unzip_docx(input_file, output_file, output_dir):
    shutil.copyfile(input_file, output_file)

    # Remove any existing files in the same directory
    for f in os.listdir(output_dir):
        os.remove(os.path.join(output_dir, f))

    # Unzip
    with ZipFile(output_file, 'r') as zipped_file:
        zipped_file.extractall(output_dir)

    os.remove(output_file)


def strip_typesetting(paragraph, debug=False):
    macron_regex = re.compile(r'([a-z])\s?\[(?:set|insert) macron over ([a-z])\]')
    umlaut_regex = re.compile(r'([a-z])\s?\[(?:set|insert) umlaut over ([a-z])\]')

    # We'll error out if there's something needed that isn't here
    macron_mapping = {
        'a': 'ā',
        'e': 'ē',
        'i': 'ī',
        'o': 'о̄',
        'u': 'ū'
    }

    # We'll error out if there's something needed that isn't here, but I only saw the single 'a'
    umlaut_mapping = {
        'a': 'ä'
    }

    # Remove the non-macron typesetting marks
    paragraph = re.sub(r'\[/?(EXT|A|B)\]\s?', '', paragraph)

    # May need to iterate over this more than once
    while macron_regex.search(paragraph):
        results = macron_regex.search(paragraph)

        # There should be the initial letter and letter inside the typesetting statement
        try:
            first_letter, second_letter = results.groups()
        except ValueError:
            raise InvalidMacronParsingError

        # They also need to be equal
        if first_letter != second_letter:
            raise InvalidMacronParsingError

        text = results.group()

        if debug:
            print("Replacing '%s' with '%s'" % (text, macron_mapping[second_letter]))

        paragraph = paragraph.replace(text, macron_mapping[second_letter])

    # May need to iterate over this more than once
    while umlaut_regex.search(paragraph):
        results = umlaut_regex.search(paragraph)

        # There should be the initial letter and letter inside the typesetting statement
        try:
            first_letter, second_letter = results.groups()
        except ValueError:
            raise InvalidUmlautParsingError

        # They also need to be equal
        if first_letter != second_letter:
            raise InvalidUmlautParsingError

        text = results.group()

        if debug:
            print("Replacing '%s' with '%s'" % (text, umlaut_mapping[second_letter]))

        paragraph = paragraph.replace(text, umlaut_mapping[second_letter])

    return paragraph


def output_fulltext(input_file, output_file, debug=False):
    doc = Document(input_file)
    if debug:
        print("Total number of paragraphs:", len(doc.paragraphs))

    blank_paragraphs = 0
    with codecs.open(output_file, 'w', 'UTF-8') as output:
        for p in doc.paragraphs:
            p = p.text.strip()
            if not p:
                blank_paragraphs += 1
            else:
                output.write(strip_typesetting(p))
                output.write('\n')

    if debug:
        print("- Blank paragraphs:", blank_paragraphs)
        print("- Paragraphs with content:", len(doc.paragraphs) - blank_paragraphs)


def output_italics(input_file, output_file, output_count=True, debug=False):
    results = {}

    doc = Document(input_file)
    for p in doc.paragraphs:

        italic_run = ''
        for r in p.runs:

            if r.italic:
                italic_run += r.text
            else:
                if italic_run:
                    if italic_run not in results:
                        results[italic_run] = 0
                    results[italic_run] += 1
                italic_run = ''

        # Might have one that hasn't been printed out yet once we end
        if italic_run:
            if italic_run not in results:
                results[italic_run] = 0
            results[italic_run] += 1

    for r in sorted(results, key=results.get):
        print(r, results[r])



def output_non_utf(input_file, output_file, debug=False):
    doc = Document(input_file)
    temp_file ='tmp-non-utf.txt'

    count = 0
    with open(temp_file, 'w') as temp, codecs.open(output_file, 'w', 'UTF-8') as output:
        for p in doc.paragraphs:
            p = p.text.strip()
            if p:
                try:
                    temp.write(strip_typesetting(p))
                except UnicodeEncodeError:
                    count += 1
                    output.write((' '.join(p.split()[:10]) + '\n'))
    os.remove(temp_file)

    if debug:
        print("Paragraphs with non-utf characters:", count)


def output_by_page(input_file, output_dir, debug=False):

    # Remove any previous copies
    for f in os.listdir(output_dir):
        os.remove(os.path.join(output_dir, f))

    page_num = 1
    previous_footnote = 0
    tree = ET.parse(input_file)

    output_name = '%s/page_%s.txt' % (output_dir, str(page_num))
    output = open(output_name, 'w', encoding='utf-8')
    footnotes = []
    paragraph = ''

    for paragraph_elem in tree.iter('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}p'):
        for elem in paragraph_elem.iter():
            tag = elem.tag.split('}')[-1]

            # This is text, start buffering it to print out
            if tag == 't':
                paragraph += elem.text

            # Save footnotes for printing at the end of the file
            elif tag == 'footnoteReference':
                footnote_id = elem.attrib['{http://schemas.openxmlformats.org/wordprocessingml/2006/main}id']
                assert int(footnote_id) == previous_footnote + 1, 'Invalid footnote order processing'
                footnotes.append(footnote_id)
                previous_footnote += 1

            # This is a line break - print out the paragraph thus far, footnotes and open the next file
            elif tag == 'lastRenderedPageBreak':
                output.write(strip_typesetting(paragraph))
                paragraph = ''

                for f in footnotes:
                    output.write('\n<footnote id=%s />' % f)
                output.close()

                page_num += 1
                footnotes = []
                output_name = '%s/page_%s.txt' % (output_dir, str(page_num))
                output = open(output_name, 'w', encoding='utf-8')


        # Going onto the next line of the file - print out the paragraph and a new line
        output.write(strip_typesetting(paragraph) + '\n')
        paragraph = ''

    if debug:
        print("Number of pages output: ", page_num)


def output_footnotes(input_file, output_dir, debug=False, verbose_debug=False):

    # Remove any previous copies
    for f in os.listdir(output_dir):
        os.remove(os.path.join(output_dir, f))

    tree = ET.parse(input_file)
    footnote_id = ''
    for elem in tree.iter():
        tag = elem.tag.split('}')[-1]
        if tag == 'footnote':
            footnote_id = elem.attrib['{http://schemas.openxmlformats.org/wordprocessingml/2006/main}id']

            # Skip -1 and 0, these aren't real footnotes
            if int(footnote_id) < 1:
                continue

            text = ''
            for t in elem.iter('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}t'):
                text += t.text

            with open('%s/footnote_%s.txt' % (output_dir, footnote_id), 'w', encoding='utf-8') as output:
                output.write(strip_typesetting(text.strip()))

            if verbose_debug:
                print('Footnote id: ', footnote_id)
                print(' - ', text)

    if debug:
        print('Number of footnotes output: ', footnote_id)


def output_footnoted_pages(pages_dir, footnotes_dir, output_dir, debug=False):

    # Remove any previous copies
    for f in os.listdir(output_dir):
        os.remove(os.path.join(output_dir, f))

    footnote_regex = re.compile('<footnote>([1-9][0-9]?)</footnote>')

    for f in os.listdir(pages_dir):
        page = get_pagenum_from_filename(f)
        with open(pages_dir + f, 'r', encoding='utf-8') as page_file, \
                open(output_dir + 'footnoted_page_%s.txt' % page, 'w', encoding='utf-8') as output:

            for line in page_file:
                footnotes_printed = False
                if not line.startswith('<footnote'):
                    # We should never see content after footnotes
                    assert footnotes_printed is False
                    output.write(line)
                else:
                    footnotes_printed = True
                    footnote_id = line.split('=')[-1].split(' ')[0]

                    # Open the footnote file
                    try:
                        with open(footnotes_dir + 'footnote_%s.txt' % footnote_id, encoding='utf-8') as footnote_file:
                            for footnote_line in footnote_file:
                                output.write(footnote_line + '\n')
                    except:
                        print('Invalid footnote #', footnote_id)


def pretty_pages(pages):
    result = ''
    previous_page = 0

    for p in sorted(pages):
        if p == (previous_page + 1) and previous_page > 0:
            # This is part of a series - remove the space and comma, add a dash
            if not result.endswith('-'):
                result = result.rstrip(' ').rstrip(',')
                result += '-'
        elif result.endswith('-'):
            # The previous series needs to close
            result += str(previous_page) + ', '
            result += str(p) + ', '
        else:
            result += str(p) + ', '

        previous_page = p

    # Need to catch an end case where we haven't ended the series because there wasn't another
    if result.endswith('-'):
        result += str(previous_page)

    return result.rstrip(', ')
