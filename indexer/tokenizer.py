import re
from nltk.tokenize import NLTKWordTokenizer, sent_tokenize


# The native class won't strip hyphens - https://github.com/nltk/nltk/blob/develop/nltk/tokenize/destructive.py
class HyphenatedTokenizer(NLTKWordTokenizer):
    LONG_DASH = (re.compile(r"—"), r" — ")

    def tokenize(self, text, convert_parentheses=False, return_str=False):
        regexp, substitution = self.LONG_DASH
        text = regexp.sub(substitution, text)
        return super().tokenize(text, convert_parentheses, return_str)


# Use the expanded tokenizer
_tokenizer = HyphenatedTokenizer()


# https://github.com/nltk/nltk/blob/develop/nltk/tokenize/__init__.py
def expanded_word_tokenize(text, language="english", preserve_line=False):
    sentences = [text] if preserve_line else sent_tokenize(text, language)
    return [
        token for sent in sentences for token in _tokenizer.tokenize(sent)
    ]